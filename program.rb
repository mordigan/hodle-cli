# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'

Bundler.require(:default)

require 'pathname'
require 'thor'

class Program < Thor
  desc 'init', 'Initialize the CLI'
  def init
    loop do
      puts 'Please type in a command(and arguments if required) or `help` to see a list of all commands.'
      print '>> '
      command, args = STDIN.gets.chomp.split(' ')

      case command
      when 'help'
        help
      when 'generate-wallet'
        generate(args)
      when 'wallet-status'
        current_wallet(args)
      else
        puts "Unknown command `#{command}`"
        help
      end
    rescue => e
      puts e.message
    end
  end

  private
  def help
    puts " * help - prints this message
 * generate-wallet <file> generates a wallet and writes down the private key to specified file
 * wallet-status <file> reads the wallet status from file and prints it to the display"
  end

  def current_wallet(file_name)
    puts generate(file_name).to_s
  end

  def generate(file_name)
    core = Core::Program.new

    pk = File.exist?(file_name) ? File.read(file_name) : ''

    key = core.load_address(pk: pk)

    File.open(file_name, 'w') { |f| f.write(key.base58) } if pk.empty?
    key
  end
end

Program.start(ARGV)
