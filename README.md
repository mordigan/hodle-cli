# Hodle CLI
[![forthebadge](https://forthebadge.com/images/badges/made-with-ruby.svg)]()
## Installation
```bash
 git clone https://gitlab.com/mordigan/hodle-cli.git; cd hodle-cli; bundle install;
 ``` 


## Usage
* `ruby program.rb init` to initialize the CLI, this will open a prompt where you can type other commands

## Commands
* `help` => shows available commands
* `generate-wallet <file>` generates a wallet and writes down the private key to the specified file
* `wallet-status <file>` reads the wallet status from file and prints it to the display(if file is doesn't exist will generate a new one)
