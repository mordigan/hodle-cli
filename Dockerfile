FROM ruby:3.0.2

WORKDIR /app

COPY . .

RUN apt update && apt install git -y

RUN gem install bundler

RUN bundle install

CMD ['ruby', 'program.rb', 'init']